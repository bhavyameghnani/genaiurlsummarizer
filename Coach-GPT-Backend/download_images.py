import os
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin, urlparse
import pdf_downloader

image_folder = "IMAGES"
if not os.path.exists(image_folder):
    os.makedirs(image_folder)

def download_image(image_url):
    """Download raster images based on URL."""
    try:
        response = requests.get(image_url, stream=True)
        response.encoding = 'utf-8' 
        image_name = os.path.basename(urlparse(image_url).path)
        image_path = os.path.join(image_folder, image_name)
        with open(image_path, "wb") as image_file:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:
                    image_file.write(chunk)
        print(f"Raster image successfully downloaded: {image_path}")
    except Exception as e:
        print(f"Error downloading raster image {image_url}: {e}")


def save_svg(svg_content, file_name):
    """Save SVG markup to a file."""
    svg_path = os.path.join(image_folder, file_name)
    # with open(svg_path, "w", encoding="utf-8") as file:
    #     file.write(svg_content)
    # print(f"SVG successfully saved: {svg_path}")

def download_images_from_url(url):
    """Download both raster images and SVGs."""
    try:
        response = requests.get(url)
        if response.status_code == 200:
            response.encoding = 'utf-8' 
            soup = BeautifulSoup(response.content, 'html.parser')
            # <img> tags for raster images
            for img_tag in soup.find_all('img', src=True):
                img_url = urljoin(url, img_tag['src'])
                if img_url.lower().endswith(('.jpg', '.png', '.gif')) and not pdf_downloader.is_visited(img_url):
                    download_image(img_url)
            # process <svg> tags
            svg_count = 0
            # for svg_tag in soup.find_all('svg'):
            #     svg_markup = str(svg_tag)
            #     svg_count += 1
            #     save_svg(svg_markup, f"image_{svg_count}.svg")
        else:
            print(f"Failed to retrieve content from {url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred while fetching content from {url}: {e}")

# try:
#     print(f"Searching for images on {base_url}")
#     download_images_from_url(base_url)
# except Exception as e:
#     print(f"An error occurred: {e}")