import requests
from urllib.parse import urljoin, urlparse
from bs4 import BeautifulSoup
import os
import json
import base64
from datetime import datetime
import urllib
from PyPDF2 import PdfReader
from pptx.util import Pt
import pptFinal
import pdf_downloader
import text_extraction
import news_logo

from flask import Flask,request,jsonify, send_file
from flask_cors import CORS, cross_origin
from langchain.text_splitter import CharacterTextSplitter
from langchain_community.document_loaders import TextLoader
from langchain_community.embeddings import OpenAIEmbeddings, HuggingFaceInstructEmbeddings
from langchain_community.vectorstores import FAISS
from langchain_community.chat_models import ChatOpenAI
from langchain.memory import ConversationBufferMemory
from langchain.chains import ConversationalRetrievalChain
from langchain_community.llms import HuggingFaceHub
from langchain_community.vectorstores import Chroma
import openai
import feedparser
import requests
import ssl

from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.shapes import MSO_SHAPE
from pptx.dml.color import RGBColor

ssl._create_default_https_context = ssl._create_unverified_context

app = Flask(__name__)
CORS(app, support_credentials=True)

api_key="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"
openai.api_key=api_key
os.environ["OPENAI_API_KEY"] = api_key

base_url = ""


#CALLS
@app.route('/')
@cross_origin(support_credentials=True)
def hello():
    return "Welcome to GENESIS"


def get_text_chunks(text):
    text_splitter = CharacterTextSplitter(
        separator="Title:",
        chunk_size=15000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = text_splitter.split_text(text)
    return chunks



def get_text_chunks_pdf(text):
    text_splitter = CharacterTextSplitter(
        separator = "\n",
        chunk_size=2000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = text_splitter.split_text(text)
    return chunks


def get_vectorstore(text_chunks):
    embeddings = OpenAIEmbeddings()
    vectorstore = FAISS.from_texts(texts=text_chunks, embedding=embeddings)
    return vectorstore


def get_conversation_chain(vectorstore):
    llm = ChatOpenAI(temperature=0.7, model_name='gpt-3.5-turbo')
    memory = ConversationBufferMemory(
        memory_key='chat_history', return_messages=True)
    conversation_chain = ConversationalRetrievalChain.from_llm(
        llm=llm,
        retriever=vectorstore.as_retriever(),
        memory=memory
    )
    return conversation_chain

global all_text
all_text=""
@app.route('/generateCompanyNewsSummary', methods=['POST'])
@cross_origin(support_credentials=True)

def generateCompanyNewsSummary():
    companyDetails = request.get_json()
    print(companyDetails)
    companyName = companyDetails['CompanyName']
    companyNewPrompt = companyDetails['CompanyNewPrompt']
    companyName = companyName.replace(" ", "%20")
    
    filename = "news_output.txt"
    news_logo.get_latest_news_rss(companyName, filename)

    text_file = open(filename, "r")
    companyNewData = text_file.read()
    text_file.close()

    text_chunks = get_text_chunks(companyNewData)
    print("---------"*50)
    print(len(text_chunks))
    vectorstore = get_vectorstore(text_chunks)
    response = get_conversation_chain(vectorstore)
    result = response(
        {"question": companyNewPrompt})
    print(result['answer'])
    return {'status': 'success', 'data': result['answer']}


@app.route('/generateComanyOverview', methods=['POST'])
@cross_origin(support_credentials=True)

def generateComanyOverview():
    userDetails = request.get_json()
    print("-"*20)
    print(userDetails)
    companyURL = userDetails['URL']
    companyOverviewPrompt = userDetails['CompanyOverviewPrompt']
    base_url = companyURL
    companyURL = str(companyURL.replace('"', ''))
    filename = "company_overview.txt"
    text_extraction.extract_and_follow_links(base_url, filename)
    news_logo.find_logo_url(base_url)

    text_file = open(filename, "r")
    companyURLData = text_file.read()
    text_file.close()

    text_chunks = get_text_chunks(companyURLData)
    vectorstore = get_vectorstore(text_chunks)
    response = get_conversation_chain(vectorstore)
    result = response(
        {"question": companyOverviewPrompt})
    print(result['answer'])
    companyoverviewdata=result['answer']
    pptFinal.createPPT(companyoverviewdata)
    return {'status': 'success', 'data': result['answer']}


@app.route('/getuserresponsefinance', methods=['POST'])
@cross_origin(support_credentials=True)
def getuserresponsefinance():

    user_queries=request.form['user_question']
    file_name=[]
    for file in  request.files.getlist('file'):
        file_name.append(file.filename)
        file.save(file.filename)

    pdf_text=pdf_downloader.get_pdf_text(file_name)

    text_chunks=get_text_chunks_pdf(pdf_text)
    print(len(text_chunks))
    vectorstore=get_vectorstore(text_chunks)
    response=get_conversation_chain(vectorstore)

    result = response({"question": user_queries})
    return {'status': 'success', 'data': result['answer']}


pdf_folder = "PDFs (in URL)"
@app.route('/generatePdfContent', methods=['POST'])
@cross_origin(support_credentials=True)
def generatePdfContent():
    response = requests.get(base_url)
    if response.status_code == 200:
        response.encoding = 'utf-8'  
        soup = BeautifulSoup(response.content, 'html.parser')
        links = soup.find_all('a', href=True)
        for link in links:
            full_link = pdf_downloader.normalize_url(urljoin(base_url, link['href']))
            print(full_link)
            current_year = datetime.now().year
            keywords = ["press_release", "pressrelease", "q4","q3","23","release", str(current_year), str(current_year - 1)]
            if full_link.lower().endswith('.pdf') and pdf_downloader.contains_keywords(full_link, keywords):
                pdf_downloader.download_file(full_link, pdf_folder, keywords)

    pdf_downloader.scrape_and_download(base_url, base_url)
    try:
        print(f"Searching for PDFs on {base_url}")
        pdf_downloader.download_pdfs_with_fallback(base_url)
    except Exception as e:
        print(f"An error occurred: {e}")

@app.route('/getuserresponse', methods=['POST'])
@cross_origin(support_credentials=True)
def getuserresponse():

    user_queries=request.form['user_question']
    file_name=[]
    for file in  request.files.getlist('file'):
        file_name.append(file.filename)
        file.save(file.filename)

    pdf_text=pdf_downloader.get_pdf_text(file_name)
    
    print("---Appending Overview------")

    filename = "company_overview.txt"
    text_file = open(filename, "r")
    companyURLData = text_file.read()
    text_file.close()

    print("---Appending News------")

    filename = "news_output.txt"
    text_file = open(filename, "r")
    companyNewsData = text_file.read()
    text_file.close()
    pdf_text=pdf_text+" "+companyURLData+" "+companyNewsData
    all_text = companyURLData+" "+companyNewsData
    
    text_chunks=get_text_chunks_pdf(pdf_text)
    print(len(text_chunks))
    vectorstore=get_vectorstore(text_chunks)
    response=get_conversation_chain(vectorstore)

    result = response({"question": user_queries})
    return {'status': 'success', 'data': result['answer']}

if __name__ == "__main__":
    app.run(port=5000, debug=True)
