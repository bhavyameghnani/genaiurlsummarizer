import feedparser
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin, urlparse


#Function for NEWS articles
def get_latest_news_rss(company_name, output_file):
    rss_url = f'https://news.google.com/rss/search?q={company_name}&hl=en-US&gl=US&ceid=US:en'
    news_feed = feedparser.parse(rss_url)
    
    with open(output_file, 'w', encoding='utf-8') as file:
        success_count = 0
        for entry in news_feed.entries:
            if success_count >= 5:
                break

            response = requests.get(entry.link)
            if response.status_code == 200:
                print(f"Title: {entry.title}")
                print(f"Link: {entry.link}\n")

                file.write(f"Title: {entry.title}\n")
                file.write(f"Link: {entry.link}\n\n")

                soup = BeautifulSoup(response.text, 'html.parser')
                for script_or_style in soup(["script", "style"]):
                    script_or_style.decompose()

                title = soup.title.text.strip() if soup.title else 'No title found'
                page_text = ' '.join(soup.get_text().split())
                # print(page_text + '\n')

                if "Read more" in page_text:
                    page_text = page_text.split("Read more")[0].strip()

                file.write(page_text + '\n\n')

                success_count += 1
            else:
                print("Failed to fetch article.")

#find logo
def find_logo_url(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            response.encoding = 'utf-8' 
            soup = BeautifulSoup(response.content, 'html.parser')
            
            logo_tags = soup.find_all('img', alt=True) + \
                        soup.find_all('img', title=True) + \
                        soup.find_all(class_='logo') + \
                        soup.find_all(class_='branding') + \
                        soup.find_all(class_='header-logo') + \
                        soup.find_all(class_='logo-image')
            
            if logo_tags:
                logo_url = urljoin(url, logo_tags[0]['src'])
                print(f"Logo URL found: {logo_url}")
                return logo_url
            else:
                print("No logo found on the webpage.")
                return None
        else:
            print(f"Failed to retrieve content from {url}. Status code: {response.status_code}")
            return None
    except Exception as e:
        print(f"An error occurred while fetching content from {url}: {e}")
        return None
