import os
import feedparser
import requests
import ssl
import requests
from urllib.parse import urljoin, urlparse
from bs4 import BeautifulSoup
from PyPDF2 import PdfReader

pdf_folder = "PDFs (in URL)"
if not os.path.exists(pdf_folder):
    os.makedirs(pdf_folder)


visited_urls = set()
def normalize_url(url):
    """Strip fragments from the URL and normalize it."""
    return urlparse(url)._replace(fragment="").geturl()

def is_visited(url):
    normalized_url = normalize_url(url)
    return normalized_url in visited_urls

def mark_as_visited(url):
    normalized_url = normalize_url(url)
    visited_urls.add(normalized_url)

def get_pdf_text(pdf_files):    
    text = ""
    for row in pdf_files:
        pdf_reader = PdfReader(row)
        for page in pdf_reader.pages:
            
            text += page.extract_text()
    return text

def contains_keywords(url, keywords):
    return any(keyword in url.lower() for keyword in keywords)

def download_file(url, folder, keywords):
    if is_visited(url):
        print(f"File already processed: {url}")
        return

    file_name = os.path.basename(urlparse(url).path)
    file_path = os.path.join(folder, file_name)

    try:
        response = requests.get(url, stream=True)
        response.encoding = 'utf-8' 
        if contains_keywords(url, keywords):
            with open(file_path, "wb") as file:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        file.write(chunk)
            print(f"File downloaded: {file_path}")
    except Exception as e:
        print(f"Error downloading {url}: {e}")

    mark_as_visited(url)


from datetime import datetime
def scrape_and_download(url, base_url, depth=0, max_depth=3):
    if depth > max_depth:
        return
    depth=depth+1
    print("DEPTH:  ", depth)

    normalized_url = normalize_url(url)
    if is_visited(normalized_url):
        return

    mark_as_visited(normalized_url)

    try:
        response = requests.get(normalized_url)
        if response.status_code == 200:
            response.encoding = 'utf-8'  
            soup = BeautifulSoup(response.content, 'html.parser')
            links = soup.find_all('a', href=True)
            for link in links:
                full_link = normalize_url(urljoin(url, link['href']))
                if full_link.lower().endswith('.pdf') and contains_keywords(full_link, keywords):
                    download_file(full_link, pdf_folder, keywords)

        else:
            print(f"Failed to retrieve content from {normalized_url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred while processing {normalized_url}: {e}")

current_year = datetime.now().year
keywords = ["press_release", "pressrelease", "release", "q4","q3","23",str(current_year), str(current_year - 1)]

def download_pdfs_with_fallback(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            response.encoding = 'utf-8' 
            soup = BeautifulSoup(response.content, 'html.parser')
            
            if soup.find_all('tr'):
                print("Table rows found. Processing PDFs within tables.")
                tables = soup.find_all('table')
                for table in tables:
                    table_rows = table.find_all('tr')
                    for row in table_rows:
                        data_cells = row.find_all('td')
                        for cell in data_cells:
                            links = cell.find_all('a', href=True)
                            for link in links:
                                full_link = urljoin(url, link['href'])
                                if full_link.lower().endswith('.pdf') and contains_keywords(full_link, keywords):
                                    print(f"Downloading PDF from table: {full_link}")
                                    download_file(full_link, pdf_folder, keywords)
            else:
                print("No table rows found. Processing all PDF links.")
                links = soup.find_all('a', href=True)
                for link in links:
                    full_link = urljoin(url, link['href'])
                    if full_link.lower().endswith('.pdf') and contains_keywords(full_link, keywords):
                        print(f"Found PDF link: {full_link}")
                        download_file(full_link, pdf_folder, keywords)
        else:
            print(f"Failed to retrieve content from {url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred while fetching content from {url}: {e}")