#create ppt
from pptx.enum.text import MSO_AUTO_SIZE
from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.shapes import MSO_CONNECTOR
from pptx.dml.color import RGBColor

def createPPT(companyinfo):
    prs = Presentation()
    slide_layout = prs.slide_layouts[6]  
    slide = prs.slides.add_slide(slide_layout)

    icons = [
        ("Automobile", "icon1.png"),
        ("Airplane", "icon32.png"),
        ("Train", "icon3.png"),
        ("Ship", "icon4.png"),
        ("Bus", "icon5.png"),
        ("Bicycle", "icon6.png"),
        ("Motorcycle", "icon7.png"),
        ("Truck", "icon42.png"),
    ]

    title_text = "Key Structure"
    title_shape = slide.shapes.add_textbox(Inches(4.7), Inches(4.2), Inches(3), Inches(0.5))
    title_shape.text = title_text
    title_shape.text_frame.paragraphs[0].font.size = Pt(16)

    profile_info = "Company Profile Information\nCompany Name: XYZ Corporation\nLocation: City, Country\nIndustry: Transportation\nOther Metrics:"
    profile_info_shape = slide.shapes.add_textbox(Inches(0.2), Inches(0.8), Inches(3), Inches(0.6))
    profile_info_shape.text_frame.text = profile_info

    for paragraph in profile_info_shape.text_frame.paragraphs:
        paragraph.font.size = Pt(13)

    for i in range(1, 4):
        x = Inches(4.5 + i * 1.5)
        y_start = Inches(4.5)
        y_end = Inches(7.4)
        line = slide.shapes.add_connector(
            MSO_CONNECTOR.STRAIGHT, x, y_start, x, y_end
        )
        line.line.color.rgb = RGBColor(0, 0, 0)  
    for i in range(2):
        y = Inches(5.9 + i * 1.5)
        x_start = Inches(4.5)
        x_end = Inches(4.5 + 4 * 1.5)
        line = slide.shapes.add_connector(
            MSO_CONNECTOR.STRAIGHT, x_start, y, x_end, y
        )
        line.line.color.rgb = RGBColor(0, 0, 0)  
    for i, (name, image) in enumerate(icons):
        left = Inches(4.7 + (i % 4) * 1.5)
        top = Inches(4.6 + (i // 4) * 1.5) 
        width = Inches(0.8)
        height = Inches(0.8)
  
        slide.shapes.add_picture(image, left, top, width, height)
     
        txBox = slide.shapes.add_textbox(left, top + height * 0.8, width, Inches(0.5))
        tf = txBox.text_frame
        p = tf.add_paragraph()
        p.text = name
        p.font.size = Pt(14)  

    # Bottom left quadrant
    left = Inches(0.2)
    top = Inches(4.2)
    width = Inches(7)
    height = Inches(3.6)

    title_shape = slide.shapes.add_textbox(left, top, width, Inches(0.5))
    title_shape.text = "Key Management"
    title_shape.text_frame.paragraphs[0].font.size = Pt(16)  

  
    ceo_image = "tarun_mehta.jpg"  
    ceo_shape = slide.shapes.add_picture(ceo_image, left + Inches(0.2), top + Inches(0.6), Inches(0.7), Inches(0.7))
    ceo_text_left = left + Inches(1.2)
    ceo_text_frame = slide.shapes.add_textbox(ceo_text_left, top + Inches(0.3), width - Inches(1.2), Inches(1.2)).text_frame
    ceo_paragraph = ceo_text_frame.add_paragraph()
    ceo_paragraph.text = "[Name of CEO]: Chief Executive Officer"
    ceo_paragraph.font.size = Pt(10)  
    ceo_paragraph.space_after = Pt(0)  

    for bullet_point in ["Bullet point 1", "Bullet point 2", "Bullet point 3"]:
        ceo_paragraph = ceo_text_frame.add_paragraph()
        ceo_paragraph.text = bullet_point
        ceo_paragraph.font.size = Pt(8) 

    cfo_top = top + Inches(1)
    cfo_image = "tarun_mehta.jpg"  
    cfo_shape = slide.shapes.add_picture(cfo_image, left + Inches(0.2), cfo_top + Inches(0.4), Inches(0.7), Inches(0.7))
    cfo_text_frame = slide.shapes.add_textbox(ceo_text_left, cfo_top, width - Inches(1.2), Inches(1.2)).text_frame
    cfo_paragraph = cfo_text_frame.add_paragraph()
    cfo_paragraph.text = "[Name of CFO]: Chief Financial Officer"
    cfo_paragraph.font.size = Pt(10)  
    cfo_paragraph.space_after = Pt(0) 
  
    for bullet_point in ["Bullet point 1", "Bullet point 2", "Bullet point 3"]:
        cfo_paragraph = cfo_text_frame.add_paragraph()
        cfo_paragraph.text = bullet_point
        cfo_paragraph.font.size = Pt(8)  
    coo_top = cfo_top + Inches(0.8)
    coo_image = "tarun_mehta.jpg"  
    coo_shape = slide.shapes.add_picture(coo_image, left + Inches(0.2), coo_top + Inches(0.4), Inches(0.7), Inches(0.7))
    coo_text_frame = slide.shapes.add_textbox(ceo_text_left, coo_top, width - Inches(1.2), Inches(1.2)).text_frame
    coo_paragraph = coo_text_frame.add_paragraph()
    coo_paragraph.text = "[Name of COO]: Chief Operating Officer"
    coo_paragraph.font.size = Pt(10)  
    coo_paragraph.space_after = Pt(0) 
    for bullet_point in ["Bullet point 1", "Bullet point 2", "Bullet point 3"]:
        coo_paragraph = coo_text_frame.add_paragraph()
        coo_paragraph.text = bullet_point
        coo_paragraph.font.size = Pt(8)  
    logo_image = "icon42.png"  
    logo_left = Inches(10 - 2)  
    logo_top = Inches(0.2)
    logo_width = Inches(0.7)
    logo_height = Inches(0.7)
    slide.shapes.add_picture(logo_image, logo_left, logo_top, logo_width, logo_height)

    prs.save("quadrant_icons_presentation.pptx")


