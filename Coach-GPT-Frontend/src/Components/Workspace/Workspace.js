import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Header from "../Home/Header";
import CssBaseline from "@material-ui/core/CssBaseline";
import Spinner from "../Spinner";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import EmojiObjectsIcon from "@material-ui/icons/EmojiObjects";
import WbIncandescentIcon from "@material-ui/icons/WbIncandescent";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ServiceCall from "../../Service/ServiceCall";
import AssistantIcon from "@material-ui/icons/Assistant";
import { clsx } from "clsx";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

import FileUpload from "react-material-file-upload";
import WorkspaceFeaturedPost from "./WorkspaceFeaturedPost";
import Avatar from "@material-ui/core/Avatar";
import AvatarGroup from "@material-ui/lab/AvatarGroup";

import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  avatarRoot: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  opacity: {
    pointerEvents: "none",
    opacity: 0.5,
  },


  mainGrid: {
    marginTop: theme.spacing(3),
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

export default function Workspace() {
  const classes = useStyles();

  const [companyOverviewPrompt, setCompanyOverviewPrompt] = useState(
    "Act as a Research specialist and Finance domain expert. Your task is to provide summary of company profile (4 to 6 lines summary, products & services, founding year, employee count), the name and 3 lines of information or summary as bullet points of Chief Executive Officer, Chief Financial Officer, and Chief Operating Officer each, news articles and their summary, reports and their summary and few more relevant articles that would be consumed by Financial analyst"

    // "Act as a Research specialist and Finance domain expert. Your task is to provide summary of company profile (4 to 6 lines summary, products & services, founding year, employee count), entire board of directors or management structure or committee members along with their designation and 1 to 2 line summary, news articles and their summary, reports and their summary and few more relevant articles that would be consumed by Financial analyst"
  );

  const [companyNewPrompt, setCompanyNewPrompt] = useState(
    "Act as a Research analyst. Your task is to provide summary of all the news articles by sharing three to four key insights for each news mentioned in the data description. Please share the output in format - Title: {value}, Link: {value}, Key Insights: {summary of description value in bullet points}"
  );

  const [companyPPTPrompt, setcompanyPPTPrompt] = useState(
    "Act as a Research Specialist. Your task is to provide the answers to the following questions regarding the company.1.Year founded in: 2. Top 3 investors of the company with the stake in %. 3. Recent news and developments in 2 lines each. 4. Key clients and 2 lines about them. 5. Number of employees working in the firm. 6. Location of Headquarters. "

    // "Act as a Research specialist and Finance domain expert. Your task is to provide summary of company profile (4 to 6 lines summary, products & services, founding year, employee count), entire board of directors or management structure or committee members along with their designation and 1 to 2 line summary, news articles and their summary, reports and their summary and few more relevant articles that would be consumed by Financial analyst"
  );

  const [loader, setLoader] = React.useState(false);
  const [ideaDetails, setIdeaDetails] = useState();
  const [elaboratedIdea, setElaboratedIdea] = useState();
  const [problemStatement, setProblemStatement] = useState();
  const [proposedSolution, setProposedSolution] = useState();
  const [innovationModules, setInnovationModules] = useState();
  const [marketResearch, setMarketResearch] = useState();
  const [keyProblem, setKeyProblem] = useState();
  const [personas, setPersonas] = useState();
  const [choachgptresponse,setchoachgptresponse]=React.useState('')
  const [file1, setFile1] = useState();
  const [file, setFile] = useState();
  const [file2, setFile2] = useState();

  const [open, setOpen] = React.useState(false);
  const [url, setURL] = React.useState();
  const [companyoverview, setCompanyOverview] = React.useState();
  const [companyname, setCompanyName] = React.useState();
  const [userquery, setuserquery] = React.useState();
  const [userqueryfinance, setuserqueryfinance] = React.useState();
  const [companyNewSummary, setCompanyNewSummary] = React.useState();
  const [prompt, setPrompt] = React.useState();

  function handleClickOpen(prompt) {
    setOpen(true);
    setPrompt(prompt);
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleIdeaField = (event) => {
    setIdeaDetails(event.target.value);
  };

  function handleCompanyURL() {
    console.log(url);
    setLoader(true);
    const data = {
      URL: url,
      CompanyOverviewPrompt: companyOverviewPrompt,
    };
    ServiceCall.generateComanyOverview(data).then((response) => {
      console.log(response.data);
      setCompanyOverview(response.data.data);
      setLoader(false);
    });
  }

  function handleCompanyName() {
    console.log(companyname);
    setLoader(true);
    const data = {
      CompanyName: companyname,
      CompanyNewPrompt: companyNewPrompt,
    };
    ServiceCall.generateCompanyNewsSummary(data).then((response) => {
      console.log(response.data);
      setCompanyNewSummary(response.data.data);
      setLoader(false);
    });
  }

  // function handleCompanyFinancialMetrics() {
  //   console.log(prompt);
  //   setLoader(true);
  //   const data = {
  //     CompanyName: companyname,
  //     CompanyNewPrompt: companyNewPrompt,
  //   };
  //   ServiceCall.generateCompanyFinancialMetrics(data).then((response) => {
  //     console.log(response.data);
  //     setCompanyNewSummary(response.data.data);
  //     setLoader(false);
  //   });
  // }

  function handleUserQueryforFinancialMetrics() {
    console.log(userqueryfinance);
    setLoader(true);

    const formData = new FormData();
    for (let i = 0; i < file1.length; i++) {
      formData.append("file", file1[i]);
    }
    formData.append("user_question", userqueryfinance);

    ServiceCall.getUserQueryFinance(formData).then((response) => {
      
      setchoachgptresponse(response.data.data);
      setLoader(false);
    });
  }


  // function storePdfFile(e)
  // {
  //   const formData = new FormData();
  //   for (let i = 0; i < file1.length; i++) {
  //     formData.append("file", file[i]);
  //   }

  // }

  function handleUserQuery() {
    console.log(userquery);
    setLoader(true);

    const formData = new FormData();
    for (let i = 0; i < file1.length; i++) {
      formData.append("file", file1[i]);
    }
    formData.append("user_question", userquery);


    ServiceCall.getUserQuery(formData).then((response) => {
      
      setchoachgptresponse(response.data.data);
      setLoader(false);
    });
  }


  const handleFileChange = (event) => {
    setFile(event);
  };

  const handleFileChange1 = (event) => {
    setFile1(event);
  };

  const handleFileChange2 = (event) => {
    setFile2(event);
  };

  const handleChange = (event) => {
    setElaboratedIdea(event.target.value);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      {loader && <Spinner></Spinner>}
      <Header title="GENESIS: Generative Engine for News, Earnings Summaries of Industry Systems" />

      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {
            "Do you wish to customise your Prompt? Prompt Engineering is the key to everything!"
          }
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <TextField
              id="outlined-multiline-static"
              // label="Your Prompt"
              multiline
              rows={6}
              fullWidth
              value={prompt}
              variant="outlined"
            />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
          {/* <Button onClick={handleClose} color="primary">
            Close
          </Button> */}
        </DialogActions>
      </Dialog>

      <main>
        <WorkspaceFeaturedPost post={mainFeaturedPost} /> <center></center>
        <Container maxWidth align="center">
          <Grid container spacing={3}>
            {/* <Grid container item xs={12} spacing={2}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid container item xs={12} spacing={2}>
                    <Grid item xs={6}>
                      <Typography color="secondary" variant="h6" gutterBottom>
                        <b>Company Website and Sector URL</b>
                      </Typography>
                      <TextField
                        id="standard-basic"
                        variant="outlined"
                        multiline
                        style={{ margin: 40, width: 500 }}
                        placeholder="CoachGPT Search - Paste URL here"
                        onChange={(e) => setURL(e.target.value)}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <IconButton>
                                <SearchIcon onClick={handleCompanyURL} />{" "}
                             
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                      <TextField
                        id="standard-basic"
                        variant="outlined"
                        multiline
                        style={{ margin: 40, width: 500 }}
                        placeholder="CoachGPT Search - Company name for fetching News Articles"
                        onChange={(e) => setCompanyName(e.target.value)}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <IconButton>
                                <SearchIcon onClick={handleCompanyName} />{" "}
                                
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <Typography color="secondary" variant="h6" gutterBottom>
                        <b>Company & Industry Quarterly Reports</b>
                      </Typography>
                      <FileUpload
                        value={file1}
                        accept="application/pdf"
                        onChange={handleFileChange1}
                      />
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid> */}
            <Grid container item xs={12} spacing={2}>
              <Grid item xs={6}>
                <Paper className={classes.paper}>
                  <Grid container item xs={12} spacing={2}>
                    <Grid item container lg={12} sm={12} md={12} style={{display:'grid'}}>
                    <TextField
                        id="standard-basic"
                        variant="outlined"
                        multiline
                        style={{display:'grid'}}
                        placeholder="Company Url"
                        onChange={(e) => setURL(e.target.value)}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <IconButton>
                                <SearchIcon onClick={handleCompanyURL} />{" "}
                             
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Grid>
                    
                    
                 
                    {/* <Grid item xs={2}>
                      <EmojiObjectsIcon
                        color="secondary"
                        fontSize="medium"
                      ></EmojiObjectsIcon>
                    </Grid>
                    <Grid item xs={10}>
                      <Typography variant="h6" gutterBottom>
                        Company Summary & Overview
                      </Typography>
                    </Grid> */}
                  </Grid>
                  
                  <TextField
                    InputLabelProps={{
                      shrink: true,
                    }}
                    id="outlined-multiline-static"
                    label="Company Summary & Overview generated by GENESIS"
                    multiline
                    rows={12}
                    fullWidth
                    value={companyoverview}
                    variant="outlined"
                    style={{marginTop:"20px"}}
                    //onChange={(e) => handleIdeaField(e)}
                  />
                  <br /> <br />
                  {/* <Grid container item xs={12} spacing={2}>
                    <Grid item xs={2}>
                      <AssistantIcon
                        color="secondary"
                        fontSize="large"
                        onClick={() => {
                          handleClickOpen(companyOverviewPrompt);
                        }}
                      />
                    </Grid>
                    <Grid item xs={10}>
                      <Button fullWidth variant="contained" color="secondary">
                        Enhance using Coach-GPT
                      </Button>
                    </Grid>
                  </Grid> */}
                </Paper>
              </Grid>

              <Grid item xs={6} style={{display:'grid'}}>
              <Paper className={classes.paper}>
                  <Grid container item xs={12} spacing={2}>
                    <Grid item container lg={12} sm={12} md={12} style={{display:'grid'}}>
                      <Typography color="secondary" variant="h6" gutterBottom>
                        <b>Company & Industry Quarterly Reports</b>
                      </Typography>
                      <FileUpload
                        value={file1}
                        accept="application/pdf"
                        onChange={handleFileChange1}
                      />
                    </Grid>
                    <TextField
                        id="standard-basic"
                        variant="outlined"
                        multiline
                        fullWidth
                        placeholder="Add prompt to search for financial metrics"
                        onChange={(e) => setuserqueryfinance(e.target.value)}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <IconButton>
                                <SearchIcon onClick={handleUserQueryforFinancialMetrics} />{" "}
                                
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Grid>
                    <Grid container item xs={12} spacing={2}>
                    <Grid item xs={5}>
                      {/* <AssistantIcon color="secondary" fontSize="large" /> */}
                    </Grid>
                    {/* <Grid item xs={2}>
                      <Button fullWidth variant="contained" color="secondary" onClick={(e)=>storePdfFile(e)} >
                              Submit                      </Button>
                    </Grid> */}
                  </Grid>
                    </Paper>
                    </Grid>
              {/* <Grid item xs={6}>
                <Paper className={classes.paper}>
                  
                  <TextField
                    InputLabelProps={{
                      shrink: true,
                    }}
                    id="outlined-multiline-static"
                    label="Key Financials Metrics of the Company generated by Coach-GPT"
                    multiline
                    rows={12}
                    fullWidth
                    value={ideaDetails}
                    variant="outlined"
                    onChange={(e) => handleIdeaField(e)}
                  />
                  <br /> <br />
                  <Grid container item xs={12} spacing={2}>
                    <Grid item xs={2}>
                      <AssistantIcon color="secondary" fontSize="large" />
                    </Grid>
                    <Grid item xs={10}>
                      <Button fullWidth variant="contained" color="secondary">
                        Enhance using Coach-GPT
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid> */}
            </Grid>
            <br /> <br />
            <Grid container item xs={12} spacing={2}>
              <Grid item xs={6}>
                <Paper className={classes.paper}>
                  {/* <Grid container item xs={12} spacing={2}>
                    <Grid item xs={2}>
                      <EmojiObjectsIcon
                        color="secondary"
                        fontSize="medium"
                      ></EmojiObjectsIcon>
                    </Grid>
                    <Grid item xs={10}>
                      <Typography variant="h6" gutterBottom>
                        Company News Article Summary
                      </Typography>
                    </Grid>
                  </Grid> */}

<Grid item container lg={12} sm={12} md={12} style={{display:'grid'}}>
                    <TextField
                        id="standard-basic"
                        variant="outlined"
                        multiline
                        style={{display:'grid'}}
                        placeholder="GENESIS Search - Company name for fetching News Articles"
                        onChange={(e) => setCompanyName(e.target.value)}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <IconButton>
                                <SearchIcon onClick={handleCompanyName} />{" "}
                                
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Grid>
                  <TextField
                    InputLabelProps={{
                      shrink: true,
                    }}
                    id="outlined-multiline-static"
                    label="Company News Article Summary generated by GENESIS"
                    multiline
                    rows={12}
                    fullWidth
                    style={{marginTop:'20px'}}
                    value={companyNewSummary}
                    variant="outlined"
                    //onChange={(e) => handleIdeaField(e)}
                  />
                  <br /> <br />
                  {/* <Grid container item xs={12} spacing={2}>
                    <Grid item xs={2}>
                      <AssistantIcon
                        color="secondary"
                        fontSize="large"
                        onClick={() => {
                          handleClickOpen(companyNewPrompt);
                        }}
                      />
                    </Grid>

                    <Grid item xs={10}>
                      <Button fullWidth variant="contained" color="secondary">
                        Download
                      </Button>
                    </Grid>
                  </Grid> */}
                </Paper>
              </Grid>
              {/* <Grid item xs={6} style={{minHeight:'395px',maxHeight:'395px',overflow:'auto'}}> */}
              <Grid item xs={6}>
                <Paper className={classes.paper}>
               
                  <TextField
                    InputLabelProps={{
                      shrink: true,
                    }}
                    id="outlined-multiline-static"
                    label="Response from CoachGPT"
                    multiline
                    rows={13}
                    fullWidth
                    // style={{marginTop:'20px'}}
                    value={choachgptresponse}
                    variant="outlined"
                    //onChange={(e) => handleIdeaField(e)}
                  />
                  
                  <Grid item container lg={12} sm={12} md={12} style={{display:'grid'}}>
                    <TextField
                        id="standard-basic"
                        variant="outlined"
                        multiline
                        style={{display:'grid',marginTop:'20px'}}
                        placeholder="Ask your queries here"

                        className={clsx({
                          [classes.opacity]: file1 === "" || file1 === undefined,
                        })}

                        

                        onChange={(e) => setuserquery(e.target.value)}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment>
                              <IconButton>
                                <SearchIcon onClick={handleUserQuery} />{" "}
                                
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </Grid>
                </Paper>
              </Grid>
            </Grid>
            <br /> <br />
          </Grid>
        </Container>
      </main>
    </React.Fragment>
  );
}

const mainFeaturedPost = {
  title: "CoachGPT: Company & Industry Profiling",
  description:
    "Research Portal revolutionizing market & industry research adoption for Investment Banking division by leveraging the power of Generative AI. ",
  image:
    "https://dappersitebuilder.com/wp-content/uploads/2023/05/ai-website-builder-dapper-sites.jpg",
  imgText: "main image description",
  linkText: "Continue reading…",
};
