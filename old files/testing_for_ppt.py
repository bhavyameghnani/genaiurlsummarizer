#create ppt
import re

from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.text import MSO_AUTO_SIZE

def createPPT(companyoverviewdata):
    prs = Presentation()
    slide_layout = prs.slide_layouts[5]  
    slide = prs.slides.add_slide(slide_layout)

    left_start = Inches(0.2)
    top_start = Inches(1.2)
    slide_width = Inches(10)  
    slide_height = Inches(7.5)
    quadrant_width = slide_width / 2 - left_start
    quadrant_height = (slide_height - top_start * 2) / 2

    # company_profile = ""
    # board_of_directors = ""
    # news_articles_summary = ""

    # lines = companyoverviewdata.split("\n")

    # in_company_profile = False
    # in_board_of_directors = False
    # in_news_articles_summary = False
    # # in_reports_summary = False

    # for line in lines:
    #     print("line: ", line, "\n")
    #     if "company" in line.lower() and not in_board_of_directors and not in_news_articles_summary:
    #         in_company_profile = True
    #         in_board_of_directors = False
    #         in_news_articles_summary = False
    #     elif ("board" in line.lower() or "director" in line.lower() or "management" in line.lower() or "team" in line.lower()) and not in_news_articles_summary:
    #         in_board_of_directors = True
    #         in_company_profile = False
    #         in_news_articles_summary = False
    #     elif ("news" in line.lower() or "relevant" in line.lower() or "articles" in line.lower() or "reports" in line.lower() or "financial" in line.lower()) and not in_company_profile:
    #         in_news_articles_summary = True
    #         in_company_profile = False
    #         in_board_of_directors = False

    
    #     if in_company_profile:
    #         company_profile += line + "\n"
    #     elif in_board_of_directors:
    #         board_of_directors += line + "\n"
    #     elif in_news_articles_summary:
    #         news_articles_summary += line + "\n"
    # print("Printing after strip:")
    # print(company_profile.strip())
    # print(board_of_directors.strip())
    # print(news_articles_summary.strip())



    company_profile="**Company Profile:**\nRakuten Group is a global tech company founded in 1997, offering a wide range of services such as e-commerce, fintech, and mobile communication. With a diverse portfolio of over 70 services, Rakuten has a significant global presence and employs a large number of individuals."
    board_of_directors="**Board of Directors / Management:**\n- Hiroshi Mikitani (Chairman, President & CEO): Leading the company since its founding.\n- Masayuki Hosaka (FinTech Segment Leader): Instrumental in the rapid growth of the FinTech business.\n- Kentaro Hyakuno (Mobile Segment Leader): Driving the growth of the Mobile Segment.\n- Kazunori Takeda: Contributing to the growth of the e-commerce business.\n- Kenji Hirose (CFO): Strengthening the company's financial base."
    news_articles_summary="*News Articles Summary:**\nRecent news highlights Rakuten's innovative initiatives in various sectors, showcasing its commitment to social impact and continuous growth.\n\n**Reports Summary:**\nRakuten's financial reports reflect steady growth and strategic investments, affirming its position as a leading tech company in the global market.\n\n**Relevant Articles for Financial Analysts:**\n- \"Analyzing Rakuten's Diversified Portfolio for Financial Insights\"\n- \"Evaluating Rakuten's Growth Strategies in the Tech Industry\"\n- \"Financial Performance Analysis: Rakuten's Market Position and Potential Growth\""
   
 # Top Left Quadrant: Company Overview
    q1_left, q1_top = left_start, top_start
    quadrant_1 = slide.shapes.add_textbox(q1_left, q1_top, quadrant_width, quadrant_height)
    tf_1 = quadrant_1.text_frame
    tf_1.text = "Company Overview:"
    tf_1.word_wrap = True
    tf_1.auto_size = MSO_AUTO_SIZE.SHAPE_TO_FIT_TEXT

    for point in company_profile.split(". "):  
        p = tf_1.add_paragraph() 
        p.text = point
        p.font.size = Pt(14)
        p.level = 0

    # Calculate maximum width and height available for the text box
    max_width = slide_width - left_start - quadrant_width
    max_height = slide_height - top_start * 2

    # Bottom Left Quadrant: Team Information with Images
    q3_left, q3_top = left_start, top_start + quadrant_height
    title_left, title_top = q3_left, q3_top
    board_title = slide.shapes.add_textbox(title_left, title_top, max_width, Inches(0.5))
    board_title.text = "Board of Directors:"

    for idx, entry in enumerate(board_of_directors.split('\n')):
        if ':' in entry:
            parts = entry.split(': ')
        elif '-' in entry:
            parts = entry.split('- ')
        elif '(' in entry:
            parts = entry.split('(')
        else:
            continue

        if len(parts) < 2:
            continue

        name = parts[0].strip()
        designation = parts[1].strip()
        image_path = "tarun_mehta.jpg"

        # Add image
        img_left = q3_left
        img_top = q3_top + (Inches(0.5) * idx)  
        img = slide.shapes.add_picture(image_path, img_left, img_top, width=Inches(0.5), height=Inches(0.5))

        name_left = q3_left + Inches(1.0)
        name_top = img_top + Inches(0.1) 
        name_text_frame = slide.shapes.add_textbox(name_left, name_top, Inches(2), Inches(0.5))
        name_text_frame.text = name
        name_text_frame.text_frame.paragraphs[0].font.size = Pt(9)  

        # Add designation
        desig_left = name_left
        desig_top = name_top + Inches(0.3)  
        desig_text_frame = slide.shapes.add_textbox(desig_left, desig_top, Inches(2), Inches(0.5))
        desig_text_frame.text = designation
        desig_text_frame.text_frame.paragraphs[0].font.size = Pt(9)

    # Top Right Quadrant: Financial Metrics
    q2_left, q2_top = left_start + quadrant_width, top_start
    quadrant_2 = slide.shapes.add_textbox(q2_left, q2_top, max_width, max_height)
    tf_2 = quadrant_2.text_frame
    tf_2.text = "Articles' summaries:"
    tf_2.word_wrap = True
    tf_2.auto_size = MSO_AUTO_SIZE.SHAPE_TO_FIT_TEXT
    for point in news_articles_summary.split("\n"):  
        p = tf_2.add_paragraph() 
        p.text = point
        p.font.size = Pt(8)
        p.level = 0

    prs.save("test_company_slides.pptx")

companyoverview = ""
createPPT(companyoverview)

def createPPT1_5(news_output):
    news_output = "Title: Warriors Announce 2024 Women’s Empowerment Month Celebrations, Presented By Rakuten\nLink: https://news.google.com/rss/articles/CBMiZ2h0dHBzOi8vd3d3Lm5iYS5jb20vd2FycmlvcnMvbmV3cy93YXJyaW9ycy1hbm5vdW5jZS0yMDI0LXdvbWVucy1lbXBvd2VybWVudC1tb250aC1jZWxlYnJhdGlvbnMtMjAyNDAzMDHSAQA?oc=5\nKey Insights:\n- The Golden State Warriors announced celebrations for Women\’s Empowerment Month presented by Rakuten.\n- Various activations are planned throughout March to empower girls and women and amplify voices.\n- Activities include reading rallies, basketball clinics, leadership programs, and community events.\n\nTitle: Rakuten Symphony: Cloud native is the future for operators - TelecomTV\nLink: https://news.google.com/rss/articles/CBMigwFodHRwczovL3d3dy50ZWxlY29tdHYuY29tL2NvbnRlbnQvc3BvdGxpZ2h0LW9uLTVnLXBhcnRuZXItaW5zaWdodHMvcmFrdXRlbi1zeW1waG9ueS1jbG91ZC1uYXRpdmUtaXMtdGhlLWZ1dHVyZS1mb3Itb3BlcmF0b3JzLTQ5ODg3L9IBhwFodHRwczovL3d3dy50ZWxlY29tdHYuY29tL2NvbnRlbnQvc3BvdGxpZ2h0LW9uLTVnLXBhcnRuZXItaW5zaWdodHMvcmFrdXRlbi1zeW1waG9ueS1jbG91ZC1uYXRpdmUtaXMtdGhlLWZ1dHVyZS1mb3Itb3BlcmF0b3JzLTQ5ODg3L2FtcC8?oc=5\nKey Insights:\n- Rakuten Symphony\'s president discussed the benefits of cloud-native telecom transformation.\n- The company highlights the advantages of cloud-native approaches over traditional strategies.\n- The conversation took place at MWC2024 and focused on the future of cloud-native for operators."
    presentation1_5 = Presentation("test_company_slides.pptx")
    slide = presentation1_5.slides[-1]    
    news_text_box = slide.shapes.add_textbox(Inches(5), Inches(3.5), Inches(5), Inches(5))
    news_text_frame = news_text_box.text_frame
    news_text_frame.word_wrap = True
    news_text_frame.auto_size = MSO_AUTO_SIZE.SHAPE_TO_FIT_TEXT
    link_index = news_output.find("Link:")
    trimmed_output = news_output
    while "Link:" in trimmed_output:
        # Find the index of the substring "Link:"
        link_index = trimmed_output.find("Link:")
        if link_index != -1:
            # Find the index of the next newline character after "Link:"
            newline_index = trimmed_output.find("\n", link_index)
            if newline_index != -1:
                # Remove the part of the string starting from "Link:" until the next newline character
                trimmed_output = trimmed_output[:link_index] + trimmed_output[newline_index+1:]
            else:
                # If newline character not found, remove "Link:" and everything after it
                trimmed_output = trimmed_output[:link_index]
    news_text_frame.text = "News Data:\n" + trimmed_output
    for paragraph in news_text_frame.paragraphs:
        for run in paragraph.runs:
            run.font.size = Pt(9)
    presentation1_5.save("test_company_slides1_5.pptx")

createPPT1_5("")

def createPPT4():
    presentation2 = Presentation("test_company_slides1_5.pptx")
    slide_layout = presentation2.slide_layouts[5]
    slide = presentation2.slides.add_slide(slide_layout)

    # Add title to the slide
    title_shape = slide.shapes.title
    title_shape.text = "Flat-icon Icons"

    # Add icons to the slide
    icon_left_offset = Inches(0.5)  
    icon_top_offset = Inches(1.5)  
    icon_paths = ["icon1.png", "icon2.jpg", "icon3.png", "icon4.png","icon5.png","icon6.png","icon7.png"]
    for icon_path in icon_paths:
        slide.shapes.add_picture(icon_path, icon_left_offset, icon_top_offset, width=Inches(1), height=Inches(1))
        icon_left_offset += Inches(1.2)  

    presentation2.save("test_ppt_w_icons.pptx")

createPPT4()
# create ppt
import re
def createPPT_old(companyoverviewdata):
    prs = Presentation()
    slide_layout = prs.slide_layouts[5]  
    slide = prs.slides.add_slide(slide_layout)

    left_start = Inches(0.2)
    top_start = Inches(1.2)
    slide_width = Inches(10)  
    slide_height = Inches(7.5)
    quadrant_width = slide_width / 2 - left_start
    quadrant_height = (slide_height - top_start * 2) / 2

    company_profile = ""
    board_of_directors = ""
    news_articles_summary = ""
    # reports_summary = ""

    lines = companyoverviewdata.split("\n")
    # lines = re.split(r'\n|\.', companyoverview)

    in_company_profile = False
    in_board_of_directors = False
    in_news_articles_summary = False
    # in_reports_summary = False

    for line in lines:
        print("line: ", line, "\n")
        if "company" in line.lower() and not in_board_of_directors and not in_news_articles_summary:
            in_company_profile = True
            in_board_of_directors = False
            in_news_articles_summary = False
        elif ("board" in line.lower() or "director" in line.lower() or "management" in line.lower() or "team" in line.lower()) and not in_news_articles_summary:
            in_board_of_directors = True
            in_company_profile = False
            in_news_articles_summary = False
        elif ("news" in line.lower() or "relevant" in line.lower() or "articles" in line.lower() or "reports" in line.lower() or "financial" in line.lower()) and not in_company_profile:
            in_news_articles_summary = True
            in_company_profile = False
            in_board_of_directors = False
        # elif "reports" in line.lower() or "financial" in line.lower():
        #     in_reports_summary = True
        #     in_news_articles_summary = False
    
        if in_company_profile:
            company_profile += line + "\n"
        elif in_board_of_directors:
            board_of_directors += line + "\n"
        elif in_news_articles_summary:
            news_articles_summary += line + "\n"
        # elif in_reports_summary:
        #     reports_summary += line + "\n"

    print("Printing after strip:")
    print(company_profile.strip())
    print(board_of_directors.strip())
    print(news_articles_summary.strip())
    # print(reports_summary.strip())
    # news_articles_summary = news_articles_summary + "\n" + reports_summary


    # Top Left Quadrant: Company Overview
    q1_left, q1_top = left_start, top_start
    quadrant_1 = slide.shapes.add_textbox(q1_left, q1_top, quadrant_width, quadrant_height)
    tf_1 = quadrant_1.text_frame
    tf_1.text = "Company Overview:"

    for point in company_profile.split(". ")[0:]:  
        p = tf_1.add_paragraph() 
        p.text = point
        p.font.size = Pt(10)
        p.level = 0  

    # Bottom Left Quadrant: Team Information with Images
    q3_left, q3_top = left_start, top_start + quadrant_height

    for idx, entry in enumerate(board_of_directors.split('\n')):
        if ':' in entry:
            parts = entry.split(': ')
        elif '-' in entry:
            parts = entry.split('- ')
        elif '(' in entry:
            parts = entry.split('(')
        else:
            continue

        if len(parts) < 2:
            continue

        name = parts[0].strip()
        designation = parts[1].strip()
        image_path = "tarun_mehta.jpg"

        # Add image
        img_left = q3_left
        img_top = q3_top + (Inches(0.5) * idx)  
        img = slide.shapes.add_picture(image_path, img_left, img_top, width=Inches(0.5), height=Inches(0.5))

        name_left = q3_left + Inches(1.0)
        name_top = img_top + Inches(0.1) 
        name_text_frame = slide.shapes.add_textbox(name_left, name_top, Inches(2), Inches(0.5))
        name_text_frame.text = name
        name_text_frame.text_frame.paragraphs[0].font.size = Pt(9)  

        # Add designation
        desig_left = name_left
        desig_top = name_top + Inches(0.3)  
        desig_text_frame = slide.shapes.add_textbox(desig_left, desig_top, Inches(2), Inches(0.5))
        desig_text_frame.text = designation
        desig_text_frame.text_frame.paragraphs[0].font.size = Pt(9)


    # Top Right Side: Articles Summaries
    q2_left, q2_top = left_start + quadrant_width, top_start
    quadrant_2 = slide.shapes.add_textbox(q2_left, q2_top, quadrant_width * 2, quadrant_height)
    tf_2 = quadrant_2.text_frame
    tf_2.text = "Summaries of Articles of the Company:"

    txBox = slide.shapes.add_textbox(q2_left, q2_top, quadrant_width * 2, quadrant_height)
    tf = txBox.text_frame
    p = tf.add_paragraph()
    p.text = news_articles_summary.strip()
    p.font.size = Pt(8)


    # # Bottom Right Quadrant: Examples of Icons/Images
    # q4_left, q4_top = left_start + quadrant_width, top_start + quadrant_height
    # icon_left_offset = q4_left  # Starting left position for icons
    # icon_paths = ["icon1.png", "icon2.jpg", "icon3.png", "icon4.png"]
    # for icon_path in icon_paths:
    #     slide.shapes.add_picture(icon_path, icon_left_offset, q4_top, width=Inches(1), height=Inches(1))
    #     icon_left_offset += Inches(1.2) 

    prs.save("company_slides.pptx")





    # company_profile="**Company Profile:**\nRakuten Group is a global tech company founded in 1997, offering a wide range of services such as e-commerce, fintech, and mobile communication. With a diverse portfolio of over 70 services, Rakuten has a significant global presence and employs a large number of individuals."
    # board_of_directors="**Board of Directors / Management:**\n- Hiroshi Mikitani (Chairman, President & CEO): Leading the company since its founding.\n- Masayuki Hosaka (FinTech Segment Leader): Instrumental in the rapid growth of the FinTech business.\n- Kentaro Hyakuno (Mobile Segment Leader): Driving the growth of the Mobile Segment.\n- Kazunori Takeda: Contributing to the growth of the e-commerce business.\n- Kenji Hirose (CFO): Strengthening the company's financial base."
    # news_articles_summary="*News Articles Summary:**\nRecent news highlights Rakuten's innovative initiatives in various sectors, showcasing its commitment to social impact and continuous growth.\n\n**Reports Summary:**\nRakuten's financial reports reflect steady growth and strategic investments, affirming its position as a leading tech company in the global market.\n\n**Relevant Articles for Financial Analysts:**\n- \"Analyzing Rakuten's Diversified Portfolio for Financial Insights\"\n- \"Evaluating Rakuten's Growth Strategies in the Tech Industry\"\n- \"Financial Performance Analysis: Rakuten's Market Position and Potential Growth\""
   