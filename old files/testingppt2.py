from pptx import Presentation
from pptx.util import Inches, Pt
from pptx.enum.shapes import MSO_CONNECTOR
from pptx.dml.color import RGBColor

def createPPT(companyinfo):
    prs = Presentation()
    slide_layout = prs.slide_layouts[6]  # Slide layout with 4 equal parts
    slide = prs.slides.add_slide(slide_layout)

    icons = [
        ("Automobile", "icons images/icon1.png"),
        ("Airplane", "icons images/icon8.png"),
        ("Train", "icons images/icon3.png"),
        ("Ship", "icons images/icon4.png"),
        ("Bus", "icons images/icon5.png"),
        ("Bicycle", "icons images/icon6.png"),
        ("Motorcycle", "icons images/icon7.png"),
        ("Truck", "icons images/icon9.png"),
    ]

    title_text = "Key Structure"
    title_shape = slide.shapes.add_textbox(Inches(4.7), Inches(4.2), Inches(3), Inches(0.5))
    title_shape.text = title_text
    title_shape.text_frame.paragraphs[0].font.size = Pt(16)

    # Add company profile information at the top of the slide
    profile_info = "Company Profile Information\nCompany Name: XYZ Corporation\nLocation: City, Country\nIndustry: Transportation"
    profile_info_shape = slide.shapes.add_textbox(Inches(0.2), Inches(0.8), Inches(3), Inches(0.6))
    profile_info_shape.text_frame.text = profile_info

    for paragraph in profile_info_shape.text_frame.paragraphs:
        paragraph.font.size = Pt(13)

    # Add dividing lines between the icons images and names in the bottom right quadrant
    for i in range(1, 4):
        x = Inches(4.5 + i * 1.5)
        y_start = Inches(4.5)
        y_end = Inches(7.4)
        line = slide.shapes.add_connector(
            MSO_CONNECTOR.STRAIGHT, x, y_start, x, y_end
        )
        line.line.color.rgb = RGBColor(0, 0, 0)  # Black color

    # Add horizontal lines to separate icon images and names
    for i in range(2):
        y = Inches(5.9 + i * 1.5)
        x_start = Inches(4.5)
        x_end = Inches(4.5 + 4 * 1.5)  # Adjusted end point
        line = slide.shapes.add_connector(
            MSO_CONNECTOR.STRAIGHT, x_start, y, x_end, y
        )
        line.line.color.rgb = RGBColor(0, 0, 0)  # Black color

    # Add icons with names underneath in the bottom right quadrant
    for i, (name, image) in enumerate(icons):
        left = Inches(4.7 + (i % 4) * 1.5)
        top = Inches(4.6 + (i // 4) * 1.5)  # Adjusted top value
        width = Inches(0.8)
        height = Inches(0.8)
        # Add image
        slide.shapes.add_picture(image, left, top, width, height)
        # Add name underneath
        txBox = slide.shapes.add_textbox(left, top + height * 0.8, width, Inches(0.5))
        tf = txBox.text_frame
        p = tf.add_paragraph()
        p.text = name
        p.font.size = Pt(14)  # Adjust font size

    # Bottom left quadrant
    left = Inches(0.2)
    top = Inches(4.2)
    width = Inches(7)
    height = Inches(3.6)
    # Add title
    title_shape = slide.shapes.add_textbox(left, top, width, Inches(0.5))
    title_shape.text = "Key Management"
    title_shape.text_frame.paragraphs[0].font.size = Pt(16)  # Adjust font size for title

    # Add images and text for CEO, CFO, and COO
    ceo_image = "tarun_mehta.jpg"  # Replace with actual CEO image file
    ceo_shape = slide.shapes.add_picture(ceo_image, left + Inches(0.2), top + Inches(0.6), Inches(0.7), Inches(0.7))
    ceo_text_left = left + Inches(1.2)
    ceo_text_frame = slide.shapes.add_textbox(ceo_text_left, top + Inches(0.3), width - Inches(1.2), Inches(1.2)).text_frame
    ceo_paragraph = ceo_text_frame.add_paragraph()
    ceo_paragraph.text = "[Name of CEO]: Chief Executive Officer"
    ceo_paragraph.font.size = Pt(10)  # Adjust font size for CEO info
    ceo_paragraph.space_after = Pt(0)  # Adjust space after CEO info
    # Add bullet points for CEO
    for bullet_point in ["Bullet point 1", "Bullet point 2", "Bullet point 3"]:
        ceo_paragraph = ceo_text_frame.add_paragraph()
        ceo_paragraph.text = bullet_point
        ceo_paragraph.font.size = Pt(8)  # Adjust font size for bullet points

    cfo_top = top + Inches(1)
    cfo_image = "tarun_mehta.jpg"  # Replace with actual CFO image file
    cfo_shape = slide.shapes.add_picture(cfo_image, left + Inches(0.2), cfo_top + Inches(0.4), Inches(0.7), Inches(0.7))
    cfo_text_frame = slide.shapes.add_textbox(ceo_text_left, cfo_top, width - Inches(1.2), Inches(1.2)).text_frame
    cfo_paragraph = cfo_text_frame.add_paragraph()
    cfo_paragraph.text = "[Name of CFO]: Chief Financial Officer"
    cfo_paragraph.font.size = Pt(10)  # Adjust font size for CFO info
    cfo_paragraph.space_after = Pt(0)  # Adjust space after CFO info
    # Add bullet points for CFO
    for bullet_point in ["Bullet point 1", "Bullet point 2", "Bullet point 3"]:
        cfo_paragraph = cfo_text_frame.add_paragraph()
        cfo_paragraph.text = bullet_point
        cfo_paragraph.font.size = Pt(8)  # Adjust font size for bullet points

    coo_top = cfo_top + Inches(0.8)
    coo_image = "tarun_mehta.jpg"  # Replace with actual COO image file
    coo_shape = slide.shapes.add_picture(coo_image, left + Inches(0.2), coo_top + Inches(0.4), Inches(0.7), Inches(0.7))
    coo_text_frame = slide.shapes.add_textbox(ceo_text_left, coo_top, width - Inches(1.2), Inches(1.2)).text_frame
    coo_paragraph = coo_text_frame.add_paragraph()
    coo_paragraph.text = "[Name of COO]: Chief Operating Officer"
    coo_paragraph.font.size = Pt(10)  # Adjust font size for COO info
    coo_paragraph.space_after = Pt(0)  # Adjust space after COO info
    # Add bullet points for COO
    for bullet_point in ["Bullet point 1", "Bullet point 2", "Bullet point 3"]:
        coo_paragraph = coo_text_frame.add_paragraph()
        coo_paragraph.text = bullet_point
        coo_paragraph.font.size = Pt(8)  # Adjust font size for bullet points

    # Add logo image at the extreme top right corner
    logo_image = "icon42.png"  # Replace with the actual logo image file
    logo_left = Inches(10 - 2)  # 10 inches is the slide width, subtracting width of image
    logo_top = Inches(0.2)
    logo_width = Inches(0.7)
    logo_height = Inches(0.7)
    slide.shapes.add_picture(logo_image, logo_left, logo_top, logo_width, logo_height)

    prs.save("quadrant_icons_presentation.pptx")
