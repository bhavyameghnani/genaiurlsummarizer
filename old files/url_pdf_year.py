import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin
import os
import warnings
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", message="Some characters could not be decoded, and were replaced with REPLACEMENT CHARACTER.*")


pdf_folder = "PDFs (in URL)"
if not os.path.exists(pdf_folder):
    os.makedirs(pdf_folder)

image_folder = "IMAGES"
if not os.path.exists(image_folder):
    os.makedirs(image_folder)


def download_pdfs_with_fallback(url):
    current_year = datetime.now().year
    # print ("\nCurrent year: ", current_year, "\n")
    keywords = ["press_release", "revenue", "policy","management","annual","return","dividend","financial","stats", "pressrelease", "release", str(current_year), str(current_year - 1)]

    try:
        response = requests.get(url)
        if response.status_code == 200:
            response.encoding = 'utf-8' 
            soup = BeautifulSoup(response.content, 'html.parser')
            
            if soup.find_all('tr'):
                print("Table rows found. Processing PDFs within tables.")
                tables = soup.find_all('table')
                for table in tables:
                    table_rows = table.find_all('tr')
                    for row in table_rows:
                        data_cells = row.find_all('td')
                        for cell in data_cells:
                            links = cell.find_all('a', href=True)
                            for link in links:
                                full_link = urljoin(url, link['href'])
                                if full_link.lower().endswith('.pdf') and contains_keywords(full_link, keywords):
                                    print(f"Downloading PDF from table: {full_link}")
                                    download_file(full_link, pdf_folder, keywords)
            else:
                print("No table rows found. Processing all PDF links.")
                links = soup.find_all('a', href=True)
                for link in links:
                    full_link = urljoin(url, link['href'])
                    if full_link.lower().endswith('.pdf') and contains_keywords(full_link, keywords):
                        print(f"Found PDF link: {full_link}")
                        download_file(full_link, pdf_folder, keywords)
        else:
            print(f"Failed to retrieve content from {url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred while fetching content from {url}: {e}")

visited_urls = set()
def normalize_url(url):
    """Strip fragments from the URL and normalize it."""
    return urlparse(url)._replace(fragment="").geturl()

def is_visited(url):
    normalized_url = normalize_url(url)
    return normalized_url in visited_urls

def mark_as_visited(url):
    normalized_url = normalize_url(url)
    visited_urls.add(normalized_url)

def download_image(image_url):
    """Download raster images based on URL."""
    try:
        response = requests.get(image_url, stream=True)
        response.encoding = 'utf-8' 
        image_name = os.path.basename(urlparse(image_url).path)
        image_path = os.path.join(image_folder, image_name)
        with open(image_path, "wb") as image_file:
            for chunk in response.iter_content(chunk_size=1024):
                if chunk:
                    image_file.write(chunk)
        print(f"Raster image successfully downloaded: {image_path}")
    except Exception as e:
        print(f"Error downloading raster image {image_url}: {e}")

def download_file(url, folder, keywords):
    if is_visited(url):
        print(f"File already processed: {url}")
        return

    file_name = os.path.basename(urlparse(url).path)
    file_path = os.path.join(folder, file_name)

    try:
        response = requests.get(url, stream=True)
        response.encoding = 'utf-8' 
        if contains_keywords(url, keywords):
            with open(file_path, "wb") as file:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        file.write(chunk)
            print(f"File downloaded: {file_path}")
        # else:
            # print(f"Skipping download for {url}: Keywords not found.")
    except Exception as e:
        print(f"Error downloading {url}: {e}")

    mark_as_visited(url)


from datetime import datetime
def scrape_and_download(url, base_url, depth=0, max_depth=5):
    if depth > max_depth:
        # print("Maximum depth reached. Aborting.")
        return
    current_year = datetime.now().year
    keywords = ["press_release", "revenue", "policy","management","annual","return","dividend","financial","stats", "pressrelease", "release", str(current_year), str(current_year - 1)]

    normalized_url = normalize_url(url)
    if is_visited(normalized_url):
        return

    mark_as_visited(normalized_url)

    try:
        response = requests.get(normalized_url)
        if response.status_code == 200:
            response.encoding = 'utf-8'  
            soup = BeautifulSoup(response.content, 'html.parser')
            links = soup.find_all('a', href=True)
            for link in links:
                full_link = normalize_url(urljoin(url, link['href']))
                if full_link.lower().endswith('.pdf') and contains_keywords(full_link, keywords) and full_link not in mark_as_visited:
                    download_file(full_link, pdf_folder, keywords)
                elif urlparse(full_link).netloc == urlparse(base_url).netloc:
                    scrape_and_download(full_link, base_url, depth=depth+1, max_depth=max_depth)
            for img_tag in soup.find_all('img', src=True):
                img_url = urljoin(url, img_tag['src'])
                if img_url.lower().endswith(('.jpg', '.png', ".gif")) and not is_visited(img_url):
                    if "placeholder" not in img_url.lower():
                        download_file(img_url, image_folder, keywords)
        else:
            print(f"Failed to retrieve content from {normalized_url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred while processing {normalized_url}: {e}")

def contains_keywords(url, keywords):
    return any(keyword in url.lower() for keyword in keywords)

def save_svg(svg_content, file_name):
    """Save SVG markup to a file."""
    svg_path = os.path.join(image_folder, file_name)
    # with open(svg_path, "w", encoding="utf-8") as file:
    #     file.write(svg_content)
    # print(f"SVG successfully saved: {svg_path}")

def download_images_from_url(url):
    """Download both raster images and SVGs."""
    try:
        response = requests.get(url)
        if response.status_code == 200:
            response.encoding = 'utf-8' 
            soup = BeautifulSoup(response.content, 'html.parser')
            # <img> tags for raster images
            for img_tag in soup.find_all('img', src=True):
                img_url = urljoin(url, img_tag['src'])
                if img_url.lower().endswith(('.jpg', '.png', '.gif')) and not is_visited(img_url):
                    download_image(img_url)
            # process <svg> tags
            svg_count = 0
            # for svg_tag in soup.find_all('svg'):
            #     svg_markup = str(svg_tag)
            #     svg_count += 1
            #     save_svg(svg_markup, f"image_{svg_count}.svg")
        else:
            print(f"Failed to retrieve content from {url}. Status code: {response.status_code}")
    except Exception as e:
        print(f"An error occurred while fetching content from {url}: {e}")

def find_logo_url_and_download(url, save_path):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            response.encoding = 'utf-8'
            soup = BeautifulSoup(response.content, 'html.parser')
            
            logo_tags = soup.find_all('img', alt=True) + \
                        soup.find_all('img', title=True) + \
                        soup.find_all(class_='logo') + \
                        soup.find_all(class_='branding') + \
                        soup.find_all(class_='header-logo') + \
                        soup.find_all(class_='logo-image')
            
            if logo_tags:
                logo_url = urljoin(url, logo_tags[0]['src'])
                print(f"Logo URL found: {logo_url}")
                
                # Download the logo image
                response = requests.get(logo_url)
                if response.status_code == 200:
                    # Save the image to the specified path
                    with open(os.path.join(save_path, 'logo_image.jpg'), 'wb') as f:
                        f.write(response.content)
                    print("Logo image downloaded successfully.")
                    return logo_url
                else:
                    print(f"Failed to download logo image from {logo_url}. Status code: {response.status_code}")
                    return None
            else:
                print("No logo found on the webpage.")
                return None
        else:
            print(f"Failed to retrieve content from {url}. Status code: {response.status_code}")
            return None
    except Exception as e:
        print(f"An error occurred while fetching content from {url}: {e}")
        return None

def main(base_url):
    # response = requests.get(base_url)

    scrape_and_download(base_url, base_url)
    logo_url = find_logo_url_and_download(base_url)
    try:
        print(f"Searching for PDFs on {base_url}")
        download_pdfs_with_fallback(base_url)
    except Exception as e:
        print(f"An error occurred: {e}")
    try:
        print(f"Searching for images on {base_url}")
        download_images_from_url(base_url)
    except Exception as e:
        print(f"An error occurred: {e}")
    
# base_url = "https://www.anmolindustries.com/"
base_url = "https://global.rakuten.com/corp/"
main(base_url)
