import requests
from urllib.parse import urljoin, urlparse
from bs4 import BeautifulSoup

def is_valid_url(base, url):
    parsed_base = urlparse(base)
    parsed_url = urlparse(url)
    domain_match = parsed_base.netloc == parsed_url.netloc
    path_match = parsed_url.path.startswith(parsed_base.path)
    
    return domain_match and path_match

def extract_links(url, already_seen):
    links = []
    try:
        response = requests.get(url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            
            for link in soup.find_all('a', href=True):
                full_link = urljoin(url, link['href'])
                keywords = ["2021","2020","2019","2018", "2017", "2016", "2015", "2014", "2013","2012", "2011", "2010", "2009", "2008","2007"]
                if ".jpg" not in full_link and "#" not in full_link and all(keyword not in full_link for keyword in keywords) and not full_link.endswith('.pdf') and not full_link.endswith('.mp3') and not full_link.endswith('.html') and is_valid_url(base_url, full_link) and full_link not in already_seen:
                    links.append(full_link)
                    already_seen.add(full_link)
    except Exception as e:
        print(f"An error occurred while fetching links from {url}: {e}")
    return links

def extract_text_from_url(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            for script_or_style in soup(["script", "style"]):
                script_or_style.decompose()
            title = soup.title.text.strip() if soup.title else 'No title found'
            page_text = ' '.join(soup.get_text().split())
            return title, page_text, url
        else:
            return "Failed to retrieve content", "Failed to retrieve content, status code: " + str(response.status_code), url
    except Exception as e:
        return "An error occurred", "An error occurred: " + str(e), url

def extract_and_follow_links(url, filename):
    if url.endswith("/") == False:
        url=url+"/"
    already_seen = set()
    titles_and_texts = []

    def recursive_extraction(url):
        title, text, url = extract_text_from_url(url)
        print(f"Title of {url}: {title}")
        titles_and_texts.append((title, text, url))
        links = extract_links(url, already_seen)
        print(f"\nFound {len(links)} links on {url}.")
        
        for link in links:
            recursive_extraction(link)

    print(f"Extracting data from {url}")
    recursive_extraction(url)

    with open(filename, 'w', encoding='utf-8') as file:
        for title, text, link in titles_and_texts:
            file.write(f"Title: {title}\n URL: {link} \n Text: {text}\n\n")

base_url = "https://groww.in/blog/"
filename = "url_TEXT.txt"
extract_and_follow_links(base_url, filename)
