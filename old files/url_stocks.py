import yfinance as yf
from urllib.parse import urlparse

def fetch_stock_prices(input_value, period="3mo"):
    company_ticker_mapping = {
        # Banking
        "jpmorgan": "JPM",
        "bankofamerica": "BAC",
        "wellsfargo": "WFC",
        "citigroup": "C",
        "goldmansachs": "GS",
        "morganstanley": "MS",
        "usbankcorp": "USB",
        "pncfinancial": "PNC",
        "americanexpress": "AXP",
        "hsbcholdings": "HSBC",
        "barclays": "BCS",
        "creditsuisse": "CS",
        "deutschebank": "DB",
        "mitsubishifinancial": "MUFG",
        "santanderbank": "SAN",
        "icbc": "IDCBY",
        "bbva": "BBVA",
        "royalbankofcanada": "RY",
        "tdbank": "TD",
        # Retail
        "walmart": "WMT",
        "amazon": "AMZN",
        "homedepot": "HD",
        "costco": "COST",
        "alibaba": "BABA",
        "target": "TGT",
        "lowes": "LOW",
        "tjxcompanies": "TJX",
        "walgreens": "WBA",
        "jdcom": "JD",
        "dollargeneral": "DG",
        "dollartree": "DLTR",
        "kroger": "KR",
        "rossstores": "ROST",
        "bestbuy": "BBY",
        "macys": "M",
        "cvshealth": "CVS",
        "lululemon": "LULU",
        "gap": "GPS",
        "nordstrom": "JWN",
        # Other Industries
        "tesla": "TSLA",
        "google": "GOOGL",  
        "facebook": "FB",
        "apple": "AAPL",
        "microsoft": "MSFT",
        "netflix": "NFLX",
        "disney": "DIS",
        "twitter": "TWTR",
        "nike": "NKE",
        "generalelectric": "GE",
        "3m": "MMM",
        "cocacola": "KO",
        "pepsico": "PEP",
        "mcdonalds": "MCD",
        "starbucks": "SBUX",
        "visa": "V",
        "mastercard": "MA",
        "uber": "UBER",
        "lyft": "LYFT",
        "airbnb": "ABNB",
    }


    if input_value.startswith("http://") or input_value.startswith("https://"):
        company_name = extract_company_name(input_value)
        symbol = company_ticker_mapping.get(company_name)
    else:
        symbol = input_value

    if symbol:
        stock = yf.Ticker(symbol)
        hist = stock.history(period=period)
        return hist
    else:
        print("Symbol not found for", input_value)
        return None

def extract_company_name(url):
    parsed_url = urlparse(url)
    domain = parsed_url.netloc

    if domain.startswith("www."):
        domain = domain[4:]  # Remove "www." if present

    parts = domain.split(".")
    company_name = parts[0]

    return company_name

input_value = "https://www.jpmorgan.com"  #either URL or symbol
stock_prices = fetch_stock_prices(input_value)
if stock_prices is not None:
    print(f"Stock prices: ({input_value})\n {stock_prices}")
    txt_file = f"Stock_prices.txt"
    with open(txt_file, "w") as f:
        f.write(stock_prices.to_string())
    print(f"Stock prices written to {txt_file}")
